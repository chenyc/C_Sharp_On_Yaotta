﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bill
{
    public partial class Form1 : Form
    {
        private static readonly double ELECT_MONEY = 2.8;
        public Form1()
        {
            InitializeComponent();
            QuickBotton butCon = new QuickBotton();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        
        public bool CheckInputIsDigits()
        {
            //Convert.ToInt32(textBox1.Text)
            int oc;
            bool isDigits = int.TryParse(textBox1.Text, out oc);
            return isDigits;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            bool chk = CheckInputIsDigits();
            int value;
            if (chk)
            {
                value = Convert.ToInt32(textBox1.Text);
                label2.Text = "本月總電費為..." + value * ELECT_MONEY;
            }
            else
                label2.Text = "請輸入數字";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            label2.Text = "請輸入總電度";
        }
    }
}
